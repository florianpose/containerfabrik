#!/bin/sh

set -e
set -v

IMAGE=${CI_REGISTRY_IMAGE}/pei:latest
echo Building ${IMAGE}...
docker build --pull \
    -t ${IMAGE} \
    -f Dockerfile.pei .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing ${IMAGE}...
    docker push ${IMAGE}
fi

IMAGE=${CI_REGISTRY_IMAGE}/alarmdisplay:latest
echo Building ${IMAGE}...
docker build --pull \
    -t ${IMAGE} \
    -f Dockerfile.alarmdisplay .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing ${IMAGE}...
    docker push ${IMAGE}
fi

IMAGE=${CI_REGISTRY_IMAGE}/flake8:latest
echo Building ${IMAGE}...
docker build --pull \
    -t ${IMAGE} \
    -f Dockerfile.flake8 .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing ${IMAGE}...
    docker push ${IMAGE}
fi
